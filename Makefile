PREFIX?=/usr/local
EXECUTABLES=epub-diff epub-text
LIBRARIES=publisher-bibebook.lua publisher-bnf.lua common-filters.lua
MANPAGES=share/man/man1/epub-diff.1 \
	 share/man/man1/epub-text.1

install: $(MANPAGES)
	install -d $(PREFIX)/bin
	for executable in $(EXECUTABLES); do			\
		install bin/$$executable $(PREFIX)/bin/;	\
	done
	install -d $(PREFIX)/lib/epub-misc
	for library in $(LIBRARIES); do							\
		install -m 644 lib/epub-misc/$$library $(PREFIX)/lib/epub-misc/;	\
	done
	install -d $(PREFIX)/share/man/man1
	for manpage in $(MANPAGES); do					\
		install -m 644 $$manpage $(PREFIX)/share/man/man1/;	\
	done
	

uninstall:
	for executable in $(EXECUTABLES); do		\
		rm -f $(PREFIX)/bin/$$executable;	\
	done
	for library in $(LIBRARIES); do			\
		rm -f $(PREFIX)/lib/epub-misc/$$library;\
	done
	rmdir $(PREFIX)/lib/epub-misc
	for manpage in $(MANPAGES); do				\
		rm -f $(PREFIX)/$$manpage;	\
	done

check:
	@tests/test_utils

%.1: %.1.md
	pandoc -s -t man $< -o $@

clean:
	for manpage in $(MANPAGES); do  \
		rm -f $$manpage;	\
	done
