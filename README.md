# epub-misc

Miscellaneous tools to interact with epub files.

## Goals
The main goal of this project is to make it easy to find differences between
multiple publications of the same book and spot copying mistakes.

## Requirements

epub-misc requires the following dependencies:

- bash (while it may be nice to support other shells, I'm not interested in
  making the code harder to read/maintain for the sole purpose of supporting
  weird shells on weird Unices)
- pandoc (to extract text from epubs)
- zip/unzip (to unpack/repack epubs in some situations)
- git (to get word-by-word diffs, I'm willing to consider other tools)

## Installation

You may install epub-misc by running:

    $ PREFIX=/path/to/prefix make install

Similarly, you may uninstall it by running:

    $ PREFIX=/path/to/prefix make uninstall

Manpages will be available in $PREFIX/share/man/man1.

## Usage

You may extract the text from an epub by running:

    $ epub-text /path/to/book.epub

You may see the differences between two epubs in your favorite pager by
running:

    $ epub-diff /path/to/book1.epub /path/to/book2.epub

Some commonly used options you may want to use:

- \-\-fix-punctuation: Try to fix punctuation as well as possible, thus removing
  some of the differences between source epubs;

- \-\-remove-diacritic-on-capitals: remove diacritics on capital letters. This
  turns "À" into "A" for instance.  Different versions of a book may use one or
  the other. This is usually not a critical issue, so users may want to hide
  such differences between epubs.

- \-\-single-dash: Always use Em dashes: some publishers use the "wrong" dashes
  (Em or En), and using only one kind of dash helps remove non-critical
  differences between source epubs.

You would then use the following command:

    $ epub-diff \
        --fix-punctuation \
        --single-dash \
        /path/to/book1.epub \
        /path/to/book2.epub

Please refer to the manual pages for more information on available options.

## Known issues

epub-text (and therefore epub-diff) may be a bit slow with certain files. It is
still faster than going to the library/bookshop, getting two copies of the same
book and comparing them by hand, though.

## License
This project is distributed under the [3-Clause BSD
License](https://opensource.org/licenses/BSD-3-Clause). See the LICENSE file.
