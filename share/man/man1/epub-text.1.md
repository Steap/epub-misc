---
title: epub-text(1)
author: Cyril Roelandt <tipecaml@gmail.com>
date: DATEPLACEHOLDER
---

# NAME
epub-text

# SYNOPSIS
epub-text [OPTION]... FILE

# DESCRIPTION
Convert an epub file to plain text.

The explicit goal is to extract the contents of the book, excluding images,
notes, credits, etc. in order to make it easy to compare two different
reproductions of the same book and spot typos.

# OPTIONS
\--fix-punctuation

: Try to fix issues regarding punctuation: for instance, replace spaces in
front of semicolons by unbreakable spaces in French texts.

-h, \--help

: Show help and return.

\--remove-diacritics-on-capitals

: Remove diacritics on capital letters. This turns "À" into "A" for instance.
Different versions of a book may use one or the other. This is usually not a
critical issue, so users may want to hide such differences between epubs,
especially when using epub-diff(1).

\--single-dash

: Replace En dashes with Em dashes. This is especially useful when used by
epub-diff(1), since the user may not care about differences in the use of
dashes. A human eye over 25 years old cannot see the difference between En and
Em dashes anyway.

# SEE ALSO
epub-diff(1)
