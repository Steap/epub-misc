---
title: epub-diff(1)
author: Cyril Roelandt <tipecaml@gmail.com>
date: DATEPLACEHOLDER
---

# NAME
epub-diff

# SYNOPSIS
epub-diff [OPTION]... FILE1 FILE2

# DESCRIPTION


# OPTIONS
\--fix-punctuation

: Try to fix issues regarding punctuation: for instance, replace spaces in
front of semicolons by unbreakable spaces in French texts.

-h, \--help

: Show help and return.

\--remove-diacritics-on-capitals

: Remove diacritics on capital letters. This turns "À" into "A" for instance.
Different versions of a book may use one or the other. This is usually not a
critical issue, so users may want to hide such differences between epubs. This
option is passed as-is to epub-text(1).

\--single-dash

: Replace En dashes with Em dashes. This is passed to epub-text(1) and makes
sure that the diff does not include differences between dashes. A human eye
over 25 years old cannot see the difference between En and Em dashes anyway.

# SEE ALSO
epub-text(1)
