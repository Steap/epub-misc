function Image(s, src, tit, attr)
    return {}
end

function CaptionedImage(src, tit, caption)
    return {}
end

function HorizontalRule(el)
    return {}
end

-- Gutenberg epubs have a small table of contents in a table, which is useless.
-- Maybe this should be part of Gutenberg-specific filter.
function Table(t)
    return {}
end
