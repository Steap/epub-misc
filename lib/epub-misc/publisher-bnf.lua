function startswith(s, prefix)
  return s:sub(1, #prefix) == prefix
end

function endswith(s, suffix)
  return s:sub(-#suffix) == suffix
end

function Div(el)
  if el.attr and el.attr.classes then
    for k, v in pairs(el.attr.classes) do
      if (v == 'figure_caption_top' or
          v == 'copyright_bookMainInfos' or
          v == 'notecall' or  -- Notes
          v == 'noteentry' or -- Contents of notes
          startswith(v, 'texte_centre')) then
        return {}
      end
    end
  end
  if (startswith(el.identifier, 'pub_') or
      endswith(el.identifier, 'apropos') or
      endswith(el.identifier, 'noteAuLecteur')) then
    return {}
  end
  return el
end

function Link(el)
  if el.attr and el.attr.classes then
    for k, v in pairs(el.attr.classes) do
      if v == 'notecall' then  -- Notes
        return {}
      end
    end
  end
  return el
end

function Note(el)
  return {}
end
