function startswith(s, prefix)
  return s:sub(1, #prefix) == prefix
end

function endswith(s, suffix)
  return s:sub(-#suffix) == suffix
end

function Para(el)
  if #el.content == 1 and el.content[1].text == 'q' then
    -- Remove ornaments at the end of chapters.
    return {}
  end
end
